import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class acuario here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class acuario extends World
{

    /**
     * Constructor for objects of class acuario.
     * 
     */
    public acuario()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        elmero elmero = new elmero();
        addObject(elmero,442,309);
        removeObject(elmero);
        tutle tutle = new tutle();
        addObject(tutle,300,501);
        tutle tutle2 = new tutle();
        addObject(tutle2,626,459);
        dolphin dolphin = new dolphin();
        addObject(dolphin,114,55);
        dolphin dolphin2 = new dolphin();
        addObject(dolphin2,743,42);
        startsea startsea = new startsea();
        addObject(startsea,74,309);
        startsea startsea2 = new startsea();
        addObject(startsea2,694,357);
        horsesea horsesea = new horsesea();
        addObject(horsesea,190,160);
        horsesea horsesea2 = new horsesea();
        addObject(horsesea2,530,278);
    }
}
